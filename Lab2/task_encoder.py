'''@file            task_encoder.py
   @brief           Defines a task to run a generalized encoder driver
   @details         This file creates a task to run an encoder driver at a regular interval. It is to be called in another function 
   @author Daniel Ceja Gonzalez
   @author Will Halvorsen
   @copyright License Info
   @date 10/6/2021
'''

import  utime
'''@brief           Imports the utime module used to control the timing scanning the position of the encoder
'''
import encoder
'''@brief           Imports the encoder driver 
'''
import pyb
'''@brief           Imports the pyb module used to control the nucleo
'''


class task_encoder:
    '''@brief           Creates a task for a general encoder
       @details         Creates a task that can be called to obtain values from a general encoder
       '''
    def __init__(self,zeropos, period,cpos1,cpos2,delta1,delta2,dqueue):
        '''@brief          Initializes the encoder task
         @details        Takes information about the encoders and data aquisition
         @param          The flag that is used to set the position of the encoders to zero
         @param          The period to be used for data aquisition
         @param          The set position of encoder 1
         @param          The set position of encoder 2
         @param          The change in position of encoder 1
         @param          The change in position of encoder 2
         @param          The queued change in encoder position
       '''
        self.enc1 = encoder.Encoder(4, pyb.Pin.cpu.B6 , pyb.Pin.cpu.B7)
        self.enc2 = encoder.Encoder(2, pyb.Pin.cpu.C6 , pyb.Pin.cpu.C7)
        self.zeropos = zeropos    #zeropos = zero_pos
        self.period = 2000
        self.cpos1 = cpos1  #encpos = curr_pos
        self.cpos2 = cpos2
        self.delta1 = delta1
        self.delta2 = delta2
        self.dqueue = dqueue
        self.nextT = utime.tick_us() + self.period
        
    def run(self):
        '''@brief           Creates a function to retreive data from an encoder
        @details         Retreives data from an encoder at a specified interval, and checks for a flag to set the position to zero
        '''
        if utime.ticks.us >= self.nextT:
            
            self.enc1.update()
            self.enc2.update()
            self.cpos1 = self.enc1.get_position
            self.cpos2 = self.enc2.get_position
            self.delta1 = self.enc1.get_delta
            self.delta2 = self.enc2.get_delta
            self.nextT+=self.period
            
            if self.zeropos.read() ==1:
                encoder.set_position(0)
                self.encpos.write(encoder.get_position())
                encoder.update()
                self.zf.write()
                
                
                
        
            
                
            
                