# -*- coding: utf-8 -*-
''' @file           task_IMU.py
    @brief          IMU task
    @details        Runs the IMU at a known frequency
    @details        File is imported by main.py
                    Click below to acces the source code:
                    https://bitbucket.org/dcejagon/me305_labs/src/master/Lab4/DRV8847.py

    @author:        Daniel Ceja
    @author:        Will Roy Halvorsen

''' 
import utime

class Task_imu:
    '''@brief           Runs the IMU at frequency freq_imu
    '''
    def __init__(self,freq_imu, BNO055, state_i ,collection,balance):
        ''' @brief     Instantiates IMU information
            @param     freq_imu The frequency to run the IMU at
            @param     BNO055 The IMU to run
            @param     state_i The queue to write orientation data to
            @param     collection The flag for data collection
            @param     balance The flag for balancing
        '''
        self.period = 1/freq_imu*1000000
        self.nextT = 0
        self.imu= BNO055
        self.balance=balance
        self.collection=collection
        self.state_i=state_i
        
        
        
    def run(self):
        ''' @brief      Retrieves IMU data at the specified frequency 
            @details    Calls the BNO055 driver to retrieve IMU to be written to state_i
        '''
        if utime.ticks_us()>self.nextT:
            self.nextT = utime.ticks_us() + self.period
            
           
            ang = self.imu.get_angle()
            vel = self.imu.get_omega()
            
            self.state_i.write((ang[1], ang[2], vel[0], vel[1]))
