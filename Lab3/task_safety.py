# -*- coding: utf-8 -*-
''' @file           task_safety.py
    @brief          Enables motors if no fault is detected, otherwise disables them
    @details        File updates to check for fault flag. 
    @details        File is imported by main.py
                    Click below to acces the source code:
                    https://bitbucket.org/dcejagon/me305_labs/src/master/Lab4/task_safety.py


    @author:        Daniel Ceja
    @author:        Will Roy Halvorsen
''' 

import utime

class SafetyTask:
    
    def __init__(self, freq, DRV8847, f_flag, c_fault, enable_motors):
        
        '''@brief   Initializes fault flag and motors
        '''
        
        self.freq = freq
        self.DRV8847 = DRV8847
        self.f_flag = f_flag
        self.c_fault = c_fault
        self.enable_motors = enable_motors
        self.period = 1 / freq *1000000
        self.nextT = 0
        
    def run(self):
        ''' @brief       Motor damage prevention
            @details     Motors are enabled once fauly flag is cleared
        '''
            
        if (utime.ticks_us() >= self.nextT):
            self.nextT = utime.ticks_us() + self.period
           
            if self.c_fault.read():
                self.DRV8847.enable()
                self.c_fault.write(0)
                self.enable_motors.write(1)
                self.f_flag.write(0)
           
            
    