# -*- coding: utf-8 -*-
'''@file            task_motor.py
    @brief          Enables motors if no fault is detected, otherwise disables them
    @details        File updates to check for fault flag. 
    @details        File is imported by main.py
                    Click below to acces the source code:
                    https://bitbucket.org/dcejagon/me305_labs/src/master/Lab4/task_motor.py


    @author:        Daniel Ceja
    @author:        Will Roy Halvorsen
''' 

import utime


class MotorTasks:
    
    def __init__(self, freq, motor, duty, f_flag):
        
        ''' @brief  Instantiates task motor information
            @param  motor number
            @param  motor frequency 
            @param  duty cycle 
            @param  Fault flag 
        '''
        
        self.freq = freq
        self.motor = motor
        self.duty = duty
        self.f_flag = f_flag
        self.period = 1 / freq * 1000000  # in us (microseconds)
        self.nextT = utime.ticks_us() + self.period
        
    def run(self):
        
        '''@brief       Retreivee encoder data as long as time continues forward
            @details    Read() and Write() mehtods used to retrieve data
            @param      calls object of itself
        '''
        
        if (utime.ticks_us() >= self.nextT):
            self.nextT = utime.ticks_us() + self.period
           
            if(self.f_flag.read()):
                self.duty.write(0)
         
            self.motor.set_duty(self.duty.read())
        