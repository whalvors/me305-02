# -*- coding: utf-8 -*-
"""@file        closedloop.py
@brief          Creates a class used to control the speed of the motors using a closed feedback loop
@details        The file creates a class that takes in data from the encoders and compares the data to the theoretical value
@details        The class then calculates the error and returns the actuation value. That is, the proportional controller times the error.
@author:        Daniel Ceja
@author:        Will Roy Halvorsen
@copyright      License Info
@date           11/16/2021
"""

class ClosedLoop:
    """@brief Creates class for the closed loop controller
       @details Creates a class to evaluate the actuation request to the motor from the proportional gain, the desired motor speed, and the measured motor speed
    """
    
    def __init__(self, mot_max, mot_min):
        """@brief Initializes the closed loop class
           @details creates the closed loop class from the initial kp value and sets motor request limits
           @param kp_init The initial kp value
           @param mot_max The motor request maximum, typically 100%
           @param mot_min The motor request minimum, typically -100%
        """
        self.mot_max = mot_max
        self.mot_min = mot_min
        
    def update(self, alg, goal, meas_speed, kp, ki, kd):
        """ @brief Creates a method to return the actuation request of the motor
            @details Compares the desired motor output to the measured output and calculates the actuation value.
            @details If the actuation value falls beyond the limits set at instantiation, the actuation value is set to the max or minimum.
            @param ref The desired or reference motor output
            @param meas The measured motor output
            @param kp The proportional gain
            @return The actuation value
        """      
        
        er = goal - meas_speed
        self.integral += er
        deriv = er - self.prev_er
        actuation = kp * er
        
        if alg == 2 or alg == 4: # CASE alg == PI or PID
            actuation += self.integral * ki
        if alg == 3 or alg == 4: # CASE alg == PD or PID
            actuation += deriv * kd
       
        if actuation > self.mot_max:
            actuation = self.mot_max
        if actuation < self.mot_min:
            actuation = self.mot_min
        return actuation
    
    def reset(self):
        """ @brief Creates a method to reset the previous error and integral to zero"""
       
        self.prev_er = 0
        self.integral = 0 
    
    def set_kp(self,kp):
        """ @brief Creates a method to set the proporitonal gain
            @param kp The proportional gain"""
        self.kp=kp
        
    def get_kp(self):
        """ @brief Creates a method to return the proportional gain
            @return The current proportional gain"""
        return self.kp
    
    def set_ki(self,ki):
        """ @brief Creates a method to set the integral gain
            @param ki The integral gain"""
        self.ki=ki
        
    def get_ki(self):
        """ @brief Creates a method to return the integral gain
            @return The current integral gain"""
        return self.ki
    
    def set_kd(self,kd):
        """ @brief Creates a method to set the differential gain
            @param kd The differential gain"""
        self.kd=kd
        
    def get_kd(self):
        """ @brief Creates a method to return the differential gain
            @return The current differential gain"""
        return self.kd
    