
'''@file            task_touch.py
    @brief          Performs scans of the resistive touch panel at a regular frequency
    @details        Task calls the touchpanel driver to scan for position readings on the touch panel
    @details        File is imported by main.py

    @author:        Daniel Ceja
    @author:        Will Roy Halvorsen
''' 


import utime
'''@brief           Imports the utime module used to control the timing
'''

class Task_touch:
    '''@brief        Runs the touch panel driver at frequency freq_input
    '''
    
    prev_state = (0,0,0)
    
    def __init__(self, freq_input, Touchpanel, balance, collection, state_t):
        ''' @brief  Instantiates task touch information
            @param  freq_input frequency to run the task driver
            @param  Touchpanel Touch Panel information 
            @param  balance balance flag 
            @param  collection collection flag
            @param  state_t queue to write position and velocity data to
        '''
        
        self.Touchpanel = Touchpanel
        self.collection = collection
        self.balance = balance
        self.freq_input = freq_input
        self.period = 1/freq_input *1000000
        self.nextT = 0
        self.state_t=state_t
        
    def run(self):
        ''' @brief      Retrieves touch panel data at the specified frequency 
            @details    Calls the toucpanel driver to retrieve position data to be written to state_t
        '''
        if utime.ticks_us()> self.nextT:
            self.nextT = utime.ticks_us() + self.period
           
            ball_vel = (0,0)
            ball_position = self.Touchpanel.read()
            if ball_position[2] == 0:
                ball_position = (0,0,0)
                self.prev_state = (0,0,0)
            else:
                if self.prev_state == (0,0,0):
                    self.prev_stae = ball_position
                else:
                    ball_vel = (((ball_position[0]-self.prev_state[0])*self.freq),((ball_position[1]-self.prev_state[1])*self.freq))
                    self.prev_state = ball_position
                    
            self.state_t.write((ball_position[0],ball_position[1],ball_vel[0],ball_vel[1],ball_position[2]))

            
            
        