# -*- coding: utf-8 -*-
"""@file        main.py
@brief          https://bitbucket.org/dcejagon/me305_labs/src/master/Lab2/
@details        Allows for multiple tasks: encoder and user, to run simultaneously
@author:        Daniel Ceja
@author:        Will Roy Halvorsen
"""
import shares
import task_user
import task_encoder        

if __name__ =='__main__':
    
    zero_pos = shares.Share(0)
    delta1 = shares.Share()
    delta2 = shares.Share()
    d_queue = shares.Queue()
    c_pos1 = shares.Share()
    c_pos2 = shares.Share()
    
    user = task_user.Task_user(zero_pos, delta1, delta2, d_queue, c_pos1, c_pos2, 100)
    updater = task_encoder.Task_encoder(zero_pos, delta1, delta2, d_queue, c_pos1, c_pos2, 100)
    
    while(True):
       
        try:
            user.run()
            updater.run
            
            
        except KeyboardInterrupt():
            break
        
print('Program Terminating')        
        
    
    
