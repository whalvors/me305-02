# -*- coding: utf-8 -*-
'''@file            task_controller.py
   @brief           Defines a task to run a controller driver
   @details         This file creates a task to run a closed loop controller driver at a regular interval
  
   @author          Daniel Ceja Gonzalez
   @author          Will Halvorsen
   @date            11/6/2021
'''
import utime
'''@brief           Imports the utime module used to control the timing
'''
from ulab import numpy as np 
'''@brief           Imports the math module used in the unit conversion of the measured angular velocity of the motor
'''

import DRV8847
'''@brief           
'''

class Controller:
    '''@brief       Motor, Balance and Collection object initialization    
       @details     Same objects as task_user
    '''
    duty1 = 0
    duty2 = 0
    
    def __init__(self, freq_input, collection, balance, stop_d, start_T, mot_1, mot_2, Gain_ks, state_i, state_t, td_queue, t_queue, i_queue):
        '''  @brief      Initializes same parameters as task user 
             @details    Some objects not pertinent to term project from
                         previous labs have been removed to improve efficiency 
        '''
        
        self.freq_input = freq_input
        self.period = 1/freq_input * 1000000
        self.nextT = 0
        self.start_T = start_T
        self.stop_d = stop_d
        self.elapsedT = 0
        
        self.balance = balance
        self.collection = collection
        self.next_c = 0
        self.motr_1 = mot_1
        self.motr_2 = mot_2
        
      
        self.Gain_ks = Gain_ks
        self.state_t = state_t      # State: Touch
        self.state_i = state_i      # State: Imu
        
        self.t_queue = t_queue      # Queue: Touch
        self.i_queue = i_queue      # Queue: IMU
        self.td_queue = td_queue    # Queue: Duty/Time
        
        
        
        
    def data_collection(self):
        '''@brief       Reads touch screen, IMU and Duty/Time Queue
           @details     Collects all needed data for ball-platform system 
        '''
        
        self.t_queue.put(self.state_t.read())
        self.i_queue.put(self.state_i.read())
        self.td_queue.put((self.elapsedT, self.duty1, self.duty2))
        
    def run(self):
        '''@brief       Updates and runs closed loop 
           @details     Continually updates the closed loop gain and speed 
                        values in addition to converting units to radians. The
                        Kx and Ky have been tuned to control the board rotation
        '''
        if utime.ticks_us() >= self.nextT:
            self.nextT = utime.ticks_us() + self.period
            
            if self.balance.read():
            
                self.elapsedT = utime.ticks_diff(utime.ticks_us(),self.start_T.read())
                
                touch = self.state_t.read()
                imu = self.state_i.read()
                
                state_x = np.array([[touch[0]/1000], [imu[0]], [touch[2]/1000], [imu[2]]])  # (x, theta_y [roll], x_dot, omega_y)
                state_y = np.array([[touch[1]/1000], [imu[1]], [touch[3]/1000], [imu[3]]])  # (y, theta_x [pitch], y_dot, omega_x)
                
                #Tune
                
                Kx =np.array([3.5, -1.75, .2, -.2])
                Ky = np.array([3.5, 1.75, .2, .2])
                
                self.duty1 = np.dot(Kx, state_x)[0]*100*2.21/(4*0.0138*12)
                self.duty2 = np.dot(Ky, state_y)[0]*100*2.21/(4*0.0138*12)
                
                self.motr_1.set_duty(self.duty1)
                self.motr_2.set_duty(self.duty2)
            else:
                self.motr_1.set_duty(0)
                self.motr_2.set_duty(0)
                
        if self.collection.read() and self.stop_d.read() == 0 and utime.ticks_us() > self.next_c:
            self.next_c=utime.ticks_us() + self.period
            self.data_collection()
                

                                    