# -*- coding: utf-8 -*-
"""@file        task_user.py
@brief          Creates user input for nucleo board
@details        The file creates a user interface that allows users to
@details        run different commands depending on the input. 
@details        File is imported by main.py
                Click below to acces the source code:
                https://bitbucket.org/dcejagon/me305_labs/src/master/Lab4/task_user.py

@author:        Daniel Ceja
@author:        Will Roy Halvorsen
"""

import utime, pyb
import array
import math

class Task_user:
    ''' @brief      User input Recognition
        @details    UI is created which allows for user inputs
    ''' 
        
    def __init__(self,zero_pos1, zero_pos2, delta1, delta2, d_queue1, d_queue2, c_pos1, c_pos2,
                 f_flag, c_fault, collection1, collection2, start_T1, start_T2,stop_T1, stop_T2, duty1, duty2, enable_motors,
                 step_1, step_2, alg_1, alg_2,kp_1, kp_2, ki_1, ki_2, kd_1, kd_2, goal_1, goal_2, freq):
        
        ''' @brief              Constructs variables of main and task_encoder
            @param              Values pertain to both encoders
        '''       
    
        self.state = 0
        self.zero_pos1 = zero_pos1            # zero position
        self.zero_pos2 = zero_pos2
        self.delta1 = delta1                   # delta 1
        self.delta2 = delta2                   # delta 2
        self.d_queue1 = d_queue1               # delta 1 queue 
        self.d_queue2 = d_queue2               # delta 2 queue 
        
        self.c_pos1 = c_pos1                   # current position 1
        self.c_pos2 = c_pos2                   # current position 2
        self.f_flag = f_flag
        self.c_fault = c_fault
      
        self.collection1 = collection1
        self.collection2 = collection2
        self.start_T1 = start_T1                # Start Time 1
        self.start_T2 = start_T2                # Start Time 2
        self.stop_T1 = stop_T1                  # Stop Time 1
        self.stop_T2 = stop_T2                  # Stop Time 2
        self.duty1 = duty1                      # Duty Motor 1
        self.duty2 = duty2                      # Duty Motor 2
        self.enable_motors = enable_motors
       
        self.step_1 = step_1
        self.step_2 = step_2
        self.alg_1 = alg_1
        self.alg_2 = alg_2
        self.kp_1 = kp_1                        # Proportional Gain 1
        self.kp_2 = kp_2                        # Proportional Gain 2
        self.ki_1 = ki_1                        # Integral Gain 1
        self.ki_2 = ki_2                        # Integral Gain 2
        self.kd_1 = kd_1                        # Derivative Gain 1
        self.kd_2 = kd_2                        # Derivative Gain 2
        self.goal_1 = goal_1
        self.goal_2 = goal_2
        
        self.t_arr1 = array.array('d',[])        # Time array 1
        self.t_arr2 = array.array('d',[])        # Time array 2
        
        self.v_arr1 = array.array('d',[])        # Velocity Array 1
        self.v_arr2 = array.array('d',[])        # Velocity Array 2
       
        self.d_arr1 = array.array('d',[])        # Duty Array 1
        self.d_arr2 = array.array('d',[])        # Duty Array 1
        
       
        self.period = 1/freq * 1000000         # In microseconds
        self.sTime = 0                         # Start Time
        self.nextT = utime.ticks_us() + self.period
        self.port = pyb.USB_VCP()
        
         
  
    def print_data(self, enc):
    
        '''@brief   retrieves and prints queue
            @return  returns 0 value
            '''


        print('\n Time (s):  Velocity (rad/s):  Effort-PWM (%):')
        
        if enc == 1:
            for i in range(0, len(self.t_arr1), 3):
                print('{0:<12}, {1:<16}, {2:<20}'.format(round(self.t_arr1[i] / 1000000, 2),
                                                         round(self.v_arr1[i] * 2.0 * math.pi / 4000 * self.freq, 2),
                                                         round(self.d_arr1[i])))
                
                
        elif enc == 2:
            for i in range(0, len(self.t_arr2), 3):
                print('{0:<12}, {1:<16}, {2:<20}'.format(round(self.t_arr2[i] / 1000000, 2),
                                                         round(self.v_arr2[i] * 2.0 * math.pi / 4000 * self.freq, 2),
                                                         round(self.d_arr2[i])))
        
        print('DATA COLLECTION COMPLETE')

    def read_duty(self):
        
        ''' @brief      Reads User Input for duty cycle 
            @details    Accounts for the user inputed data and illogical inputs
            @param      Duty cycle
        '''
        
        
        duty = ""
        place = 0
        user_in = ""
        
        while(user_in != "\r" and user_in != "\n"):
            if(self.port.any()):
                user_in = self.port.read(1).decode()

                if (user_in.isdigit()):
                    duty += user_in
                    self.port.write(user_in)
                    place += 1
                    
                elif(user_in == "-"):
                    if(place == 0):
                        duty += user_in
                        self.port.write(user_in)
                        place += 1
                    else:
                        continue
                    
                elif(user_in == "."):
                    if("." in duty):
                        continue
                    else:
                        duty += user_in
                        self.port.write(user_in)
                        
                elif(user_in == "\x7F"):
                    if(place == 0):
                        continue
                    else:
                        duty = duty[:-1]
                        self.port.write('\x7F')
                        place -= 1

        return float(duty)     
     
        
    def clear_arrays(self, enc):
        
        if enc == 1:
            self.t_arr1 = array.array('d',[])
            self.v_arr1 = array.array('d',[])
            self.d_arr1 = array.array('d',[])
            
        else: 
            self.t_arr2 = array.array('d',[])
            self.v_arr2 = array.array('d',[])
            self.d_arr2 = array.array('d',[])
            

    def run(self):
        
            ''' @brief      Runs Task User
                @details    Wait mode until an input from user is detected
                @details    and exexuted
            '''    
           
            if (utime.ticks_us()>= self.nextT):
                
                self.nextT = utime.ticks_us() + self.period
                
                '''@brief Code below is executed if the encoder time is running
                '''
                    
                if (self.state == 0):
                    '''@brief State 0 Displays commands and transitions to State 1
                    '''
                    
                    print('Welcome to Nucleo \n'
                          ' Press z to zero the position of encoder 1 \n'
                          ' Press Z to zero the position of encoder 2 \n'
                          ' Press p to print out the position of encoder 1 \n'
                          ' Press P to print out the position of encoder 2 \n'
                          ' Press d to print out the delta for encoder 1 \n'
                          ' Press D to print out the delta for encoder 2 \n'
                          ' Press m to enter the duty cycle for motor 1 \n'
                          ' Press M to enter the duty cycle for motor 1 \n'
                          ' Press c or C to clear a fault condition triggered by the DRV8847 \n'
                          ' Press g to collect encoder 1 data for 30 seconds \n'
                          ' Press G to collect encoder 2 data for 30 seconds \n'
                          ' Press s to end data collection prematurely for encoder 1 \n'
                          ' Press S to end data collection prematurely for encoder 2 \n'
                          ' Press 1 for Step Response on motor 1'
                          ' Press 2 for Step Response on motor 2'
                          ' Press Ctrl + C to Terminate Program')
                    
                    self.state = 1
                    
                elif (self.state == 1):
                    
                  '''@brief     Waiting for command inputs 
                     @param     One of the 6 inputs above
                     @details   Once one of the 6 inputs is detected the 
                     @details   Proper code is executed
                  '''
                  
                  if self.stop_T1.read():
                      self.collection1.write(0)
                      self.step_1.write(0)
                      self.stop_T1.write(0)
                      self.print_data(1)
                     
                elif self.collection1.read():
                    
                    while self.d_queue1.num_in() > 0:
                        tupl = self.d_queue1.get()
                        self.t_arr1.append(tupl[0])
                        self.v_arr1.append(tupl[1])
                        self.d_arr1.append(tupl[2])
                        
                if self.stop_T2.read():
                      self.collection2.write(0)
                      self.step_2.write(0)
                      self.stop_T2.write(0)
                      self.print_data(1)
                     
                elif self.collection2.read():
                    
                    while self.d_queue2.num_in() > 0:
                        tupl = self.d_queue2.get()
                        self.t_arr2.append(tupl[0])
                        self.v_arr2.append(tupl[1])
                        self.d_arr2.append(tupl[2])
                        
                if (self.f_flag.read()):
                      print('Fault! Press c or C to clear')
                      self.collection1.write(0)
                      self.collection2.write(0)
                      self.state = 2
                    
                elif (self.port.any()):    
                      u_input = self.port.read(1).decode()  # 1
                      
                      if u_input ==('z'):
                          print('Zeroing Encoder 1')
                          self.zero_pos1.write(True)
                          
                      elif u_input ==('Z'):
                          print('Zeroing Encoder 2')
                          self.zero_pos2.write(True)
                          
                      elif u_input ==('p'):
                          print("Encoder 1: t = {:}, p = {:}".format(utime.ticks_ms(), self.c_pos1.read()))
                      elif u_input ==('P'):
                          print("Encoder 2: t = {:}, p = {:}\n".format(utime.ticks_ms(), self.c_pos2.read()))
                        
                      elif u_input == ('d'):
                            print("Encoder1: t = {:}, d = {:}".format(utime.ticks_ms(), self.delta1.read()))
                            
                      elif u_input == ('D'):
                           
                            print("Encoder 2: t = {:}, d = {:}\n".format(utime.ticks_ms(), self.delta2.read()))
                            
                   
                      elif u_input == ('m'):
                          print('Enter Duty Cycle for Motor 1:')
                          d_cycle = self.read_duty()
                          print (' Motor 1 Duty Cycle set at: ' +str(d_cycle))
                          self.duty1.write(int(d_cycle))
                          
                      elif u_input == ('M'):
                          print('Enter Duty Cycle for Motor 2:')
                          d_cycle = self.read_duty()
                          print (' Motor 1 Duty Cycle set at: ' +str(d_cycle))
                          self.duty2.write(int(d_cycle))
                          
                      elif u_input == ('1'):
                          alg = 1
                          print("Enter Kp for Motor 1 [PWM%/(rad/s)]: \n")
                          kp = self.read_duty()
                          print('Kp is set to: ' + str(kp) +'\n')
                          print('Enter and set Velocity [rad/s] \n')
                          self.goal_1.write(self.read_duty())
                          print('Velocity is set to: ' + str(self.goal_1.read()) +'\n')
                          print('Step Response Running \n')
                          self.alg_1.write(alg)
                          self.kp_1.write(kp)
                          
                          while self.d_queue1.num_in():
                              self.d_queue1.get()
                          self.clear_arrays(1)
                          self.start_T1.write(utime.ticks_us())
                          self.step_1.write(1)
                          self.collection1.write(1)
                          
                      elif u_input == ('2'):
                          alg = 1
                          print("Enter Kp for Motor 2 [PWM%/(rad/s)]: \n")
                          kp = self.read_duty()
                          print('Kp is set to: ' + str(kp) +'\n')
                          print('Enter and set Velocity [rad/s] \n')
                          self.goal_2.write(self.read_duty())
                          print('Velocity is set to: ' + str(self.goal_2.read()) +'\n')
                          print('Step Response Running \n')
                          self.alg_2.write(alg)
                          self.kp_2.write(kp)
                          
                          while self.d_queue2.num_in():
                              self.d_queue2.get()
                          self.clear_arrays(2)
                          self.start_T2.write(utime.ticks_us())
                          self.step_2.write(1)
                          self.collection2.write(1)
                     
                      elif self.collection1.read() == 0 and u_input == ('g'):
                        print("START MOTOR #1 DATA COLLECTION \n")
                        self.collection1.write(1)
                        while self.d_queue1.num_in():
                            self.d_queue1.get()
                        self.clear_arrays(1)
                        self.start_T1.write(utime.ticks_us())
                        
                      elif self.collection2.read() == 0 and u_input == ('G'):
                        print("START MOTOR #2 DATA COLLECTION \n")
                        self.collection2.write(1)
                        while self.d_queue2.num_in():
                            self.d_queue2.get()
                        self.clear_arrays(1)
                        self.start_T2.write(utime.ticks_us())
                    
                      elif self.collection1.read() == 1 and u_input == ('s'):
                        self.collection1.write(0)
                        self.step_1.write(0)
                        self.stop_T1.write(0)
                        self.print_data(1)
                        
                    
                      elif self.collection2.read() == 1 and u_input == ('S'):
                        self.collection2.write(0)
                        self.step_2.write(0)
                        self.stop_T2.write(0)
                        self.print_data(2)
          
               
                elif (self.state == 2):
                    
                    '''@brief State 2 indicates fault condition then transitions to State 1
                    '''
                    
                    if (self.port.any()):
                        
                        u_input = self.port.read(1).decode()
                        
                        if u_input == ('c') or u_input == ('C'):
                            print("FAULT CLEARED\n")
                            self.clr_fault.write(1)
                            self.state = 1
                            self.step_1.write(0)
                            self.step_2.write(0)
                            self.duty1.write(0)
                            self.duty2.write(0)
                            self.stop_T1.write(1)
                            self.stop_T2.write(1)
