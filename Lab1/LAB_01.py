# -*- coding: utf-8 -*-
'''@file            LAB_01.py
   @brief           Cycles through a variety of LED brightness signals by pushing button B1
   @details         The program opens with a welcome message and prompts the user for input via button B1. Upon
                    pressing the button, the program cycles through led patterns: square, sawtooth, and sine waves
   @author Daniel Ceja Gonzalez
   @author Will Halvorsen
   @copyright License Info
   @date 10/6/2021
'''
import utime
'''@brief           Imports the utime module to be implemented in controlling the waveform of the LED
'''
import pyb
'''@brief           Imports the pyb module used to control the nucleo
'''
import math
'''@brief           Imports the math module used to create the sine wave function for the LED sine wave
'''
##@brief Creates a variable for the LED pin
##
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)

##@brief Creates a variable for the button input
##
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)


##@brief the button will be set as True upon pressing the button
##
button = False
def onButtonPressFCN(IRQ_src):
    '''@brief           Takes the button press input
       @details         Upon button press, this function sets the value of the global variable "button" to True
       @param           Call back function that runs when button is pressed
    '''
    global button
    button = True

def update_timer(startTime):
    '''@brief       Records the duration of the code
       @param       Takes the input from utime to update the timer
       @return      Function returns the elapsed time from the start of wave pattern
    '''
    return utime.ticks_diff(utime.ticks_ms(), startTime)

def update_sqw(t):
    '''@brief       Updates LED brightness depending on time variable "t"
       @param       Inputs elapsed time "t"
       @return      returns a value 1 or 0 to use as brightness at the LED
    '''
    return 100 * ((t % 1) < 0.5)

def update_stw(t):
    '''@brief       LED brightness depending on time variable "t"
       @param       Inputs elapsed time "t"
       @return      As t increases linearly, it will be reset to 0 when t is 1, following a sawtooth pattern
    '''
    return 100 * ((t % 1))

def update_sw(t):
    '''@brief       LED brightness depending on time variable "t"
       @param       Inputs elapsed time "t"
       @return      Returns a value following a sin wave to control the LED brightness
    '''
    return 100 * (0.5 * math.sin(math.pi * t / 5 ) + 0.5)

if __name__ == '__main__':
    ## The next state  to run as the FSM
    state = 0
    ## The number of iterations of the FSM
    runs = 0
    ## Connects the physical pressing of the button to the input as True
    ButtonInt = pyb.ExtInt(pinC13,mode=pyb.ExtInt.IRQ_FALLING, 
                           pull=pyb.Pin.PULL_NONE,callback=onButtonPressFCN)
    
    ## Creates a variable used to record the start time of each cycle
    startTime = utime.ticks_ms()
    
    #stopTime = utime.ticks_ms()
    #duration = utime.ticks_diff(stopTime, startTime)
    
    #elapsedTime = utime.ticks_ms()  # << Not sure if needed ^^
    
    ## creates a variable to use PWM to set the brightness of the LED
    LEDtimer = pyb.Timer(2, freq = 20000)
    ## Sets the brightness of the LED using PWM
    LEDbrightness = LEDtimer.channel(1, pyb.Timer.PWM, pin = pinA5)
    
    while(True):
        try:   
            if (state == 0):
                # run in state 0  
                print('Welcome. Press button, B1 on the Nucleo, to cycle through LED patterns.')
                print('Press button to begin selection')
                state = 1           # Transition to state 1
           
            
            elif (state == 1):
                # run in state 1 
                # wait for button input
                
                if button == True:
                    state = 2     # Transition to state 2
                    print('Square wave selected')
                    button = False
                
                         
            
            elif (state == 2):
                # run in state 2 
                # Square wave code
                
                elapsed = update_timer(startTime)/1000
                bright = update_sqw(elapsed)
                LEDbrightness.pulse_width_percent(bright)
                
                if button == True:
                    state = 3     # Transition to state 3
                    print('Sawtooth wave selected')
                    button = False
           
            elif (state == 3):
                # run in state 3 
                # Sawtooth wave code
                
                elapsed = update_timer(startTime)/1000
                bright = update_stw(elapsed)
                LEDbrightness.pulse_width_percent(bright)
                
                if button == True:
                    state = 4     # Transition to state 4
                    print('Sine wave selected')
                    button = False
       
            elif (state == 4):
            # run in state 4 
            # Sine wave code 
            
                elapsed = update_timer(startTime)/1000
                bright = update_sw(elapsed)
                LEDbrightness.pulse_width_percent(bright)
            
                if button == True:
                    state = 2     # Transition to state 2
                    print('Square wave selected')
                    button = False
                    startTime = utime.ticks_ms()
                
               
           
          #  runs += 1               # Increment run counter
          #  time.sleep(1)           # Delay 1 second
            
        except KeyboardInterrupt:
            break
        
print('Program Terminating')


    