''' @file           touch_panel.py
    @brief          Touch Panel Driver
    @details        Performs various scans of the touch panel to measure x and y positions 
    @details        File is imported by main.py


    @author:        Daniel Ceja
    @author:        Will Roy Halvorsen

''' 
from pyb import Pin
from pyb import ADC
import utime 
from ulab import numpy as np

class Touch_Panel:
    ''' @brief      A touch panel class
        @details    Objects of this class can be used to scan positions from the touch panel
    '''
    
    kxx = 170/4095
    kxy = 0
    kyx = 0
    kyy = 100/4095
    offset_x = -100
    offset_y = -58
    
    
    def __init__(self,P_ym,P_xm,P_yp,P_xp):
        ''' @brief      Initilizes touch panel object from the input pins
            @param  P_ym ym Pin
            @param  P_xm xm Pin
            @param  P_yp yp Pin
            @param  P_xp xp Pin
        '''
        
        self.P_ym = P_ym
        self.P_xm = P_xm
        self.P_yp = P_yp
        self.P_xp = P_xp
        
    def xy_scan(self):
        ''' @brief      Performs xy scan
            @details    Sets pins to proper values and returns ADC values for x and y positions
            @return    ADC values for x and y positions
        '''
        
        xp = Pin(self.P_xp, Pin.OUT_PP)
        xp.value(1)
        xm = Pin(self.P_xm, Pin.OUT_PP)
        xm.value(0)
        yp = Pin(self.P_yp, Pin.IN)
        ym = ADC(self.P_ym)
        ADCx = ym.read()
        yp = Pin(self.P_yp, Pin.OUT_PP)
        yp.value(1)
        ym = Pin(self.P_ym, Pin.OUT_PP)
        ym.value(0)
        xp = Pin(self.P_xp, Pin.IN)
        xm = ADC(self.P_xm)
        ADCy = xm.read()
        
        return(ADCx, ADCy)

    def x_scan(self):
        ''' @brief      Performs X scan
            @details    Sets pins to proper values for x position scan
            @return     Returns x position from ADC value altered by calibration values
        '''
        ADC = self.xy_scan()
        return ADC[0]  * self.kxx + ADC[1] * self.kxy + self.offset_x()
      
    def y_scan(self):
        ''' @brief      Performs Y scan
            @details    Sets pins to proper values for y position scan
            @return     Returns y position from ADC value altered by calibration values
        '''
        ADC = self.xy_scan()
        return ADC[0] * self.kyx + ADC[1] * self.kyy + self.offset_y

    def z_scan(self):
        ''' @brief      Performs Z scan
            @details    Sets pins to poreper values for z position scan
            @return     Returns a value for whether or not there is screen contact
        '''
        yp = Pin(self.P_yp, Pin.OUT_PP)
        yp.value(1)
        xm = Pin(self.P_xm, Pin.OUT_PP)
        xm.value(0)
        
        xp = Pin(self.P_xp, Pin.IN)
        
        
        ym = ADC(self.P_ym)
        return ym.read() < 4000

    
    def read(self):
        ''' @brief      Performs a scan in all axes
            @details    Sets pins to poreper values for x,y, then z position scans
            @return     Returns x,y,z position from ADC value altered by calibration values
        '''
        ADC = self.xy_scan()
        return ( ADC[0] * self.kxx + ADC[1] * self.kxy + self.offset_x,
                 ADC[0] * self.kyx + ADC[1] * self.kyy + self.offset_y,
                 self.z_scan())
    
    def calibrate(self):
        ''' @brief      Checks for calibration coefficients or writes own
            @details    Sets pins to poreper values for scans
            @return     Returns calibration coefficients 
        '''
        
        print("CALIBRATING TOUCH SCREEN.\n")

        filename = "RT_cal_coeffs.txt"

        try:
            with open(filename, 'r') as f:
                cal_data_string = f.readline()
                calib = [float(cal_value) for cal_value in cal_data_string.strip().split(',')]
                self.kxx = calib[0]
                self.kxy = calib[1]
                self.kyx = calib[2]
                self.kyy = calib[3]
                self.offset_x = calib[4]
                self.offset_y = calib[5]

        except:
            print("FOLLOW INSTRUCTIONS \n"
                  "Center of touch screen is (0,0)")

            points = np.array([[0,0], [80, 40], [-80, 40], [-80, -40], [80, -40]])

            ADC = np.ones((5,3))

            for i in range(5):
                print("Touch point #{:}: ({:}, {:})".format(i, points[i,0], points[i,1]))
                while True:
                    if self.z_scan():
                        xy = self.xy_scan()
                        ADC[i,0] = xy[0]
                        ADC[i,1] = xy[1]
                        while True:
                            if not self.z_scan():
                                break
                        break

            # Calibration MATRIX MATH
            calib = np.dot(np.dot(np.linalg.inv(np.dot(ADC.transpose(), (ADC))), (ADC.transpose())), (points))

            self.kxx = calib[0,0]
            self.kyx = calib[0,1]
            self.kxy = calib[1,0]
            self.kyy = calib[1,1]
            self.offset_x = calib[2,0]
            self.offset_y = calib[2,1]

            with open(filename, 'w') as f:
                f.write(f"{self.k_xx}, {self.k_xy}, {self.k_yx}, {self.k_yy}, {self.x_offset}, {self.y_offset}\r\n")

        print("\nKxx: {:}, \tKyx: {:}\n"
              "Kxy: {:}, \tKyy: {:}\n"
              "Xc:  {:}, \tYc:  {:}\n".format(self.kxx, self.kyx, self.kxy, self.kyy, self.offset_x, self.offset_y))

        print("TOUCH SCREEN CALIBRATION DONE")


if __name__ == "__main__":
    ym = Pin(Pin.cpu.A0)
    xm = Pin(Pin.cpu.A1)
    yp = Pin(Pin.cpu.A6)
    xp = Pin(Pin.cpu.A7)
    length = 176
    width = 100
    
    tch = Touch_Panel(ym,xm,yp,xp,length, width, 84,50)
    while True:
        try:
            #if tch.z_scan() == True:
                print (tch.read())
                utime.sleep_ms(60)
#            else:
#                print()
#                utime.sleep_ms(60)
            
            
        except KeyboardInterrupt:
            break
#    startT = utime.ticks_us()
#    for n in range (100):
#   #     tch.read()
#         tch.x_scan()
#    timing = (utime.ticks_us() - startT)/100
#    print(timing)
#        
        
        