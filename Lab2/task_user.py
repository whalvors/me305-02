# -*- coding: utf-8 -*-
"""@file        Task_user
@brief          Creates user input for nucleo board
@details        The file creates a user interface that allows users to
@details        run different commands depending on the input.
@author:        Daniel Ceja
@author:        Will Roy Halvorsen
"""

import utime, pyb

class Task_user:
    ''' @brief      User interface Created
        @details    UI is created which allows for inputs
    ''' 
        
    def __init__(self, zero_pos, delta1, delta2, d_queue, curr_pos1, curr_pos2, freq):
        ''' @brief              Constructs variables of main and task_encoder
            @param              Values pertain to both encoders
        '''       
    
        self.state = 0
        self.zero_pos = zero_pos               # zero position
        self.delta1 = delta1                   # delta 1
        self.delta2 = delta2                   # delta 2
        self.d_queue = d_queue                 # delta queue
        self.c_pos1 = c_pos1                   # current position 1
        self.c_pos2 = c_pos2                   # current position 2
        self.period = 1/freq * 1000000         # in microseconds
        self.sTime = 0                         #start Time
        self.n_time = utimes.ticks_us() + period
        self.port = pyb.USB_VCP
        
    def empty_queue(self, queue):
        '''@brief   Queue is reset
           @return  0 value is returned
        '''
        
        while queue.num_in() > 0:
            queue.get()
        return 0
    
    def print_queue(self, queue):
        '''@brief   Fetches queue and prints it
           @return  0 value is returned
        '''
        while queue.num_in() > 0:
            print('E1: d = ' + str(queue.get()))
        return 0


    def run(self):
        ''' @brief      Runs Task User
            @details    In wait mode until an input from user is detected
            @details    and exexuted
        '''    
            if (utime.ticks_us()>= self.n_time):
                '''@brief Code below is executed if the encoder time is running
                '''
                    
                if (self.state == 0):
                    '''@brief State 0 Displays commands and transitions to State 1
                    '''
                    
                    print('Welcome to Nucleo'
                          'Press z to zero the position'
                          'Press p to print out the position of encoder'
                          'Press d to print out the delta for encoder'
                          'Press g to collect encoder data for 30 seconds'
                          'Press s to end data collection prematurely')
                    
                    self.state = 1
                    
                elif (self.state == 1):
                  '''@brief     Waiting for command inputs 
                     @param     One of the 6 inputs above
                     @details   Once one of the 6 inputs is detected the 
                     @details   proper code is executed
                  '''
                    
                  if (self.port.any()):
                      
                      u_input = self.port(1).decode()  # 1
                      
                      if u_input ==('z'):
                          print('Zeroing Encoders')
                          self.zero_pos.write(True)
                          
                      elif u_input ==('p'):
                          print("E1: t = {:}, p = {:}".format(utime.ticks_ms(), self.c_pos1.read()))
                          print("E2: t = {:}, p = {:}\n".format(utime.ticks_ms(), self.c_pos2.read()))
                        
                      elif user_input == ('d'):
                            print("E1: t = {:}, d = {:}".format(utime.ticks_ms(), self.delta1.read()))
                            print("E2: t = {:}, d = {:}\n".format(utime.ticks_ms(), self.delta2.read()))
                            
                      elif user_input == ('g'):  
                          print(' ----Data Collection Start---- ')
                          self.state = 2
                          self.sTime = utime.ticks.ms()
                          self.empty_queue(self.d_queue)
                          
                elif (self.state == 2):
                    '''@brief       Records data for 30 seconds
                       @details      State is used to stop data collection
                    '''
                    
                    if((utime.ticks_ms() - self.sTime) < 30000): # 30 Seconds 
                        self.d_queue.put(self.delta1.read())
                        if (self.port.any()):
                            u_input = self.port.read(1).decode()
                            if u_input = ('s'):                 # 's' command
                                self.state = 3
                            else:
                                self.state = 2
                        else:
                            self.state = 2
                    else:
                        self.state = 3 
                elif (self.state ==3):
                    '''@brief   State where data collection is finished
                    '''
                    self.print_queue(self.d_queue)
                    print(" DATA COLLECTION COMPLETE")
                    self.state = 1
                    
                self.n_time += self.period

