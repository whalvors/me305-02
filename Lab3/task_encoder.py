'''@file            task_encoder.py
   @brief           Defines a task to run a generalized encoder driver
   @details         This file creates a task to run an encoder driver at a regular interval. 
                    File is imported by main.py
                    Click below to acces the source code:
                    https://bitbucket.org/dcejagon/me305_labs/src/master/Lab4/task_encoder.py
   
   @author          Daniel Ceja Gonzalez
   @author          Will Halvorsen
   @date            10/6/2021
   
'''

import  utime

class Task_encoder:
    
    '''
    @brief  Creates an object of it self for encoder tasks         
    '''
    def __init__(self, enc, freq, zero_pos, c_pos, delta, duty, collection, stop_T, d_queue, start_T):
        
        '''
        @brief          Initializes the encoder information
        @param          parameters used in the main.py file
       '''

        self.enc = enc   
    
        self.zero_pos = zero_pos        # Zero Position
        self.c_pos = c_pos              # Current Position

        self.delta = delta
        self.duty = duty
        self.d_queue = d_queue          # Data Queue
        self.collection = collection
        
    
        self.stop_T = stop_T
        self.start_T = start_T
        self.freq = freq 
        self.period = 1/ freq * 1000000 # microsecond    
        self.nextT = utime.ticks_us() + self.period
        
    def run(self):
        
        
        if utime.ticks_us() >= self.nextT:
            '''
            @brief      Retreivee encoder data as long as time continues forward
            @details    Read() and Write() mehtods used to retrieve data
            @param      calls object of itself
            '''
            self.nextT = utime.ticks_us() + self.period
            
            if (self.zero_pos.read()):
                self.enc.set_position(0)
                self.zero_pos.write(False)
            
            if(self.collection.read()):
                self.collection_data()
                
            self.enc.update()
        
            self.c_pos.write(self.enc.get_position())
            self.delta.write(self.enc.get_delta())
            
      
            
      
        
    def collection_data(self):
           
            Time_elap = utime.ticks_diff(utime.ticks_us(), self.start_T.read())
            if Time_elap >= 30000000:
                self.collection.write(0)
                self.stop.write(1)
            
            else:
                d_tuple = (Time_elap, self.delta.read(), self.duty.read())
                self.d_queue.put(d_tuple)
                
                
                
        
            
                
            
                