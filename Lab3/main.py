# -*- coding: utf-8 -*-
"""@file        main.py
@brief          Instatiates variables and run tasks simultaneously
@details        Click below to acces the source code:
                https://bitbucket.org/dcejagon/me305_labs/src/master/Lab4/main.py

@author:        Daniel Ceja
@author:        Will Roy Halvorsen
"""
import encoder
import task_encoder
import task_user
import task_motor
import task_safety
import DRV8847
import pyb
import shares
import closedloop
import task_controller

if __name__ == '__main__':
    
    '''
    @brief       Instantiates values from encoder
    '''

    
    zero_pos1 = shares.Share(0)
    zero_pos2 = shares.Share(0) 
    c_pos1 = shares.Share()
    c_pos2 = shares.Share()
    delta1 = shares.Share()
    delta2 = shares.Share()
    duty1 = shares.Share()
    duty2 = shares.Share()
    collection1 = shares.Share()
    collection2 = shares.Share()
    start_T1 = shares.Share()
    start_T2 = shares.Share()
    stop_T1 = shares.Share()
    stop_T2 = shares.Share()
    f_flag = shares.Share(0)
    c_fault = shares.Share(1)
    enable_motors = shares.Share(1)
    d_queue1 = shares.Queue()
    d_queue2 = shares.Queue()
    
    step_1 = shares.Share(0)
    step_2 = shares.Share(0)
    alg_1 = shares.Share(1)
    alg_2 = shares.Share(1)
    kp_1 = shares.Share(0)                       # Proportional Gain 1
    kp_2 = shares.Share(0)                       # Proportional Gain 2
    ki_1 = shares.Share(0)                       # Integral Gain 1
    ki_2 = shares.Share(0)                       # Integral Gain 2
    kd_1 = shares.Share(0)                       # Derivative Gain 1
    kd_2 = shares.Share(0)                       # Derivative Gain 2
    goal_1 = shares.Share()
    goal_2 = shares.Share()
 
    freq = 50
    
    DRV8847 = DRV8847.DRV8847(3, pyb.Pin(pyb.Pin.cpu.A15), pyb.Pin(pyb.Pin.cpu.B2), f_flag)
    mot_1 = DRV8847.motor(1)                     # Motor 1
    mot_2 = DRV8847.motor(2)                     # Motor 2
    enc1 = encoder.Encoder(4, pyb.Pin(pyb.Pin.cpu.B6), pyb.Pin(pyb.Pin.cpu.B7))
    enc2 = encoder.Encoder(8, pyb.Pin(pyb.Pin.cpu.C6), pyb.Pin(pyb.Pin.cpu.C7))
    c_loop1 = closedloop.ClosedLoop(100, -100)   # Closed Loop 1
    c_loop2 = closedloop.ClosedLoop(100, -100)   # Closed Loop 2
   
    user_init = task_user.Task_user(zero_pos1, zero_pos2, delta1, delta2, d_queue1, d_queue2, c_pos1, c_pos2, f_flag, c_fault, 
                                    collection1, collection2, start_T1, start_T2, stop_T1, stop_T2, 
                                    duty1, duty2, enable_motors, step_1, step_2, alg_1, alg_2, kp_1, ki_1, ki_2, kp_2, kd_1, kd_2, goal_1, goal_2, freq)
    
    motor1_read = task_encoder.Task_encoder(enc1, freq, zero_pos1, c_pos1, delta1, duty1, collection1, stop_T1, d_queue1, start_T1)
    motor2_read = task_encoder.Task_encoder(enc2, freq, zero_pos2, c_pos2, delta2, duty2, collection2, stop_T2, d_queue2, start_T2)
    
    motor1_spin = task_motor.MotorTasks(freq, mot_1, duty1, f_flag)
    motor2_spin = task_motor.MotorTasks(freq, mot_2, duty2, f_flag)
    Faultyy  = task_safety.SafetyTask(freq, DRV8847, f_flag, c_fault, enable_motors)
    con_1 = task_controller.Controller(freq, step_1, c_loop1, duty1, delta1, alg_1, start_T1, stop_T1, goal_1, kp_1, ki_1, kd_1)
    con_2 = task_controller.Controller(freq, step_2, c_loop2, duty2, delta2, alg_2, start_T2, stop_T2, goal_2, kp_2, ki_2, kd_2)
   
    while(True):
           try:
               '''
               @brief       Run tasks simultaneously 
               '''
           
               motor1_read.run()
               motor2_read.run()
               motor1_spin.run()
               motor2_spin.run()
               Faultyy.run()
               
               con_1.run()
               con_2.run()
               
               user_init.run()
           except KeyboardInterrupt:
                break
    print('Ending Program')