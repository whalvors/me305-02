# -*- coding: utf-8 -*-
"""@file        task_user.py
@brief          Creates user input for nucleo board
@details        The file creates a user interface that allows users to
@details        run different commands depending on the input. 
@details        File is imported by main.py
                Click below to acces the source code for lab 4:
                https://bitbucket.org/dcejagon/me305_labs/src/master/Lab4/task_user.py
                Click below to access most recent the source code for Term Project:
                https://bitbucket.org/dcejagon/me305_labs/src/master/Term_Project/task_user.py

@author:        Daniel Ceja
@author:        Will Roy Halvorsen
@date:          December 9, 2021

"""

import utime, pyb
import array

class Task_user:
    ''' @brief      User input Recognition
        @details    UI is created which allows for user inputs
    ''' 
        
    def __init__(self, start_T, stop_d, td_queue, t_queue, i_queue, balance, collection,freq_user):
        
        ''' @brief              Passing collection and ball balancing with shares and queues
            @param              Start and Stop times
            @param              Queues for Time and Duty
            @param              Queues for IMU and touch panel
            @param              Balance object
            @param              Collection objection
            @param              Frequency for controller to take user input
        '''       
    
        self.state = 0
        self.period = 1/freq_user * 1000000         # In microseconds
        self.sTime = 0                              # Start Time
        self.ctime = utime.ticks_us()               # Current time
        self.nextT = utime.ticks_us() + self.period
        self.port = pyb.USB_VCP()
        self.balance = balance
        self.collection = collection
        self.start_T = start_T                      # Start Time 
        self.stop_d = stop_d                        # Stop Data
      
        self.td_queue = td_queue                    # duty/time queue 
        self.t_queue = t_queue                      # touch queue 
        self.i_queue = i_queue                      # imu queue

        # Arrays 
        self.t_arr = array.array('d',[])            # Time array 
        
        self.arr_x = array.array('d',[])            # X Array 
        self.arr_y = array.array('d',[])            # Y Array 
       
        self.arr_x_vel = array.array('d',[])        # X Velocity Array 
        self.arr_y_vel = array.array('d',[])        # Y Velocity Array 
        
        self.arr_x_theta = array.array('d',[])      # Theta X Array 
        self.arr_y_theta = array.array('d',[])      # Theta Y Array
        
        self.arr_x_omega = array.array('d',[])      # Omega X Array 
        self.arr_y_omega = array.array('d',[])      # Omega Y Array
        
        self.arr_PWM_1 = array.array('i',[])        # X Array 
        self.arr_PWM_2 = array.array('i',[])        # Y Array
       
  
    def print_data(self):
    
        '''@brief   Retrieves and prints data arrays 
           @return  returns 0 value
        '''


        print(
            "\nTime (s):,   X (mm):,  Y (mm):, X_vel (mm/s):, Y_vel (mm/s):, Theta_x (rad):, Theta_y (rad):, Omega_x (rad/s):, Omega_y (rad/s):, PWM_1 (%):, PWM_2 (%):")
        for i in range(0, len(self.time_array)):
             print("{:<12}, {:<12}, {:<12}, {:<12}, {:<12}, {:<12}, {:<12}, {:<12}, {:<12}, {:<12}, {:<12}"
                  .format(round(self.t_arr[i] / 1000000, 3),
                          round(self.arr_x[i], 2),
                          round(self.arr_y[i], 2),
                          round(self.arr_x_vel[i], 2),
                          round(self.arr_y_vel[i], 2),
                          round(self.arr_x_theta[i], 2),
                          round(self.arr_y_theta[i], 2),
                          round(self.arr_x_omega[i], 2),
                          round(self.arr_y_omega[i], 2),
                          round(self.arr_PWM_1[i]),
                          round(self.arr_PWM_2[i])))
       
            
       
        print("DATA COLLECTION FINISHED  - {:02d}:{:02d}:{:02d} ----------------------------------------------"
              '\n'.format(utime.localtime()[3], utime.localtime()[4], 
                          utime.localtime()[5]), end='')

        
    def clear_arrays(self):
        '''@brief       Initializes each data collection arrays
        '''
        
        self.t_arr = array.array('d',[])            # Time array 
        
        self.arr_x = array.array('d',[])            # X Array 
        self.arr_y = array.array('d',[])            # Y Array 
       
        self.arr_x_vel = array.array('d',[])        # X Velocity Array 
        self.arr_y_vel = array.array('d',[])        # Y Velocity Array 
        
        self.arr_x_theta = array.array('d',[])      # Theta X Array 
        self.arr_y_theta = array.array('d',[])      # Theta Y Array
        
        self.arr_x_omega = array.array('d',[])      # Omega X Array 
        self.arr_y_omega = array.array('d',[])      # Omega Y Array
        
        self.arr_PWM_1 = array.array('f',[])        # X Array 
        self.arr_PWM_2 = array.array('f',[])        # Y Array
        

    
    def clear_queues(self):
        ''' @brief      Empties queues for data collection
            @details    Function clears queues for next data collection
                        avoiding queue remainder values
        '''
        
        while self.t_queue.num_in():
            self.t_queue.get()
        while self.td_queue.num_in():
            self.td_queue.get()
        while self.i_queue.num_in():
            self.i_queue.get()
      
        

    def run(self):
        
            ''' @brief      Runs Task User
                @details    Wait mode until an input from user is detected
                @details    and exexuted
            '''    
           
            if (utime.ticks_us()>= self.nextT):
                
                self.nextT = utime.ticks_us() + self.period
                
                '''@brief Code below is executed if the encoder time is running
                '''
                    
                if (self.state == 0):
                    '''@brief State 0 Displays commands and transitions to State 1
                    '''
                    
                    print('Welcome to Nucleo \n'
                          ' Press z to start balancing the ball \n'
                          ' Press x to stop balancing the ball \n'
                          ' Press c to collect data for the IMU and Touch Panel  \n'
                          ' Press s to end data collection \n'
                          ' Press Ctrl + C to Terminate Program')
    
                    self.state = 1
                    
                elif (self.state == 1):
                    
                  '''@brief     Waiting for command inputs 
                     @param     One of the 5 inputs above
                     @details   Once one of the 5 inputs is detected the 
                     @details   Proper code is executed
                  '''
                if self.collection.read():
                    
                    while self.t_queue.num_in() > 0:
                        ''' @brief      Positional and Angular values appended
                                        to tuples
                        '''
                        
                      
                        touch = self.t_queue.get()
                        imu = self.i_queue.get()
                        time_duty = self.td_queue.get()
                        
                        self.t_arr.append(time_duty[0])
                        self.arr_x.append(touch[0])
                        self.arr_y.append(touch[1])
                        self.arr_x_vel.append(touch[2])
                        self.arr_x_vel.append(touch[3])
                        self.arr_y_theta.append(imu[0])
                        self.arr_y_theta.append(imu[1])
                        self.arr_x_omega.append(imu[2])
                        self.arr_y_omega.append(imu[3])
                        self.arr_PWM_1.append(time_duty[1])
                        self.arr_PWM_2.append(time_duty[2])
                
                elif self.stop_d.read():
                    self.collection.write(0)
                    self.stop_d.write(0)
                    self.print_data()
                  
                if (self.port.any()):    
                      
                    u_input = self.port.read(1).decode()
                      
                    if u_input == ('x'):
                        '''@brief   'x' Stops balancing board
                        '''
                        self.balance.write(0)
                        print("BALANCING has stopped")

                    elif u_input == ('z'):
                        '''@brief   'z' Starts balancing board
                        '''
                
                        self.balance.write(1)
                        print("BALANCING has begun")
                        
                    elif self.collection.read() == 0 and u_input == ('c'):
                        '''@brief    'c' Starts Collecting Data 
                        '''
                        
                        print(
                            'COLLECTING BALANCING DATA - {:02d}:{:02d}:{:02d} ----------------------------------------------'
                            '\n'.format(utime.localtime()[3], utime.localtime()[4], utime.localtime()[5]), end='')
                        self.collection.write(1)
                        self.clear_queues()
                        self.clear_arrays()
                        self.start_T.write(utime.ticks_us())
                        
                    elif self.collection.read() == 1 and u_input == ('s'):
                        '''@brief       's' Stops Collecting Data
                        '''
                        
                        self.collection.write(0)
                        self.print_data()
                        
                  