'''@file            encoder.py
   @brief           Defines a driver to run a generalized encoder
   @details         This file creates a driver to run any encoder. It is to be called in another function 
   @author Daniel Ceja Gonzalez
   @author Will Halvorsen
   @copyright License Info
   @date 10/6/2021
'''
import pyb
'''@brief           Imports the pyb module used to control the nucleo
'''

class Encoder:
    '''@brief           Creates a class for a general encoder
       @details         Encoder creates a class that can be called to represent a general encoder
       '''
    
    rev = 0
    current_pos = 0
    delta = 0
    def __init__(self,timnum,pin1,pin2): 
      '''@brief          Initializes the encoder class
         @details        Takes the timer number and the pin numbers to be used for the encoder to create the encoder class
         @param          timnum The timer number for the encoder
         @param          pin1 The pin to be used for channel 1 of the encoder
         @param          pin2 The pin to be used for channel 2 of the encoder
       '''
   
      self.tim = pyb.Timer(timnum, period = 65535, prescaler = 0)
      self.timch1 = self.tim.channel(1, pyb.Timer.ENC_A, pin = pin1)
      self.timch2 = self.tim.channel(2, pyb.Timer.ENC_B, pin = pin2)

    
    def update(self):
        '''@brief           Updates the position of the encoder
           @details         Creates a method to update the position of the encoder and correct for over and underflow
        '''
      
        prev_pos = self.current_pos % 65535
        self.delta =  self.tim.counter() - prev_pos
       
    
        if self.delta > 32768:      # Underflow Condition 
            self.delta -= 65535
        elif self.delta < -32768:   # Overflow Condition  
            self.delta += 65535
        
        self.current_pos += self.delta
    def get_position(self):
        '''@brief           Returns the encoder position
           @return           The encoder position
        '''
        return self.current_pos
    
    def set_position(self,position):
        '''@brief           Sets the position of the encoder
         @details         Sets the position of the encoder to the user specified position
         @param           position The user specified position 
        '''
        self.tim.counter(0)
        self.current_pos = position
        
    def get_delta(self):
        '''@brief           Returns the change in encoder position
           @return          The change in encoder position
        '''
        
        return self.delta
