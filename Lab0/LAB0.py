# -*- coding: utf-8 -*-
"""
Created on Thu Sep 23 12:16:48 2021

@file fibonacci.py
@author: Will Halvorsen
"""



# This function calculates the Fibonacci number at the index "idx." By iteratively 
# adding the previous numbers in the sequence until the for loop reaches idx. 

def fib (idx):
    if idx<=1:
        return idx
    else:
        f0 = 0
        f1 = 1
        for num in range(1,idx):
            
            f2 = f0 + f1
            f0 = f1
            f1=f2
            
        return f2
    
# The user interface of this code will ask for a number input or the letter "q" to quit
# the code will check the input and display appropriate responses if the input is not 
# a positive integer. After checking the input, the program will output the returned
# Fibonacci number from the fib program above in a sentence.    
    
    
if __name__ == '__main__':   
    # The program will repeatedly run until "q" is input
     while(True):
        x=input('Enter a number or press q to exit the program:')
        try:
            # This line will check if the input is an integer
            idx = int(x)
        except:
            # If the input is not an integer and "q" the program will quit
            if x == 'q':
                   break
            else:
            # If the input is not an integer but not "q" the program will respond
            # asking for an integer
                   print('Input must be an integer')
        else:
            if idx<0:
            # If the input is negative, the program will display the error
            # that the input must be positive
                   print('Number must be positive')
            else:
            # If the input is a positive integer, the program will print the 
            # output of the Fibonacci function
                   print('Fibonacci number at ''index {:} is {:}' .format(idx,fib(idx)))
     # After "q" is input, the program will announce that the program is quitting
     print('Program Quitting')  
        

        