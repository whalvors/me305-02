# -*- coding: utf-8 -*-
"""
Created on Mon Nov  8 16:48:27 2021

@author: halvo
"""
from pyb import I2C
#import struct
import utime
#import array
#import binascii




class BNO055:
    
    def __init__(self, i2c):
        self.i2c = i2c
        self.angle = bytearray(6)            #Preallocate arrays to store data
        self.omeg = bytearray(6)
        self.coef = bytearray(22)
        self.stat = bytearray(1)
  
        
#Test Units
    def select_u(self, units):
        mode = self.i2c.mem_read(1, 40, 0x3D)
        self.i2c.mem_write(0, 40, 0x3D)
        self.i2c.mem_write(units, 40, 0x3B)
        self.i2c.mem_write(mode, 40, 0x3D)



    def update(self,mode):
        self.i2c.mem_write(mode,0x28,0x3D) 
        
    def calib_stat(self):
        self.i2c.mem_read(self.stat,0x28,0x35)
        return self.stat[0]
    
    def readcalib_coef(self):
        self.i2c.mem_read(self.coef,0x28,0x55)
        
    def writecalib_coef(self):
        self.i2c.mem_write(self.coef,0x28,0x55)
        
    def get_angle(self):
        self.i2c.mem_read(self.angle, 0x28, 0x1A)           #Write data to array
        eul_ang = [0,0,0]
        eul_ang[0] = self.angle[1]<<8|self.angle[0]
        eul_ang[1] = self.angle[3]<<8|self.angle[2] 
        eul_ang[2] = self.angle[5]<<8|self.angle[4]
        for i in range(3):
            if eul_ang[i] > 32767:
                eul_ang[i] -= 65536

        print('Euler Angles (deg): Heading: {: >10}    Pitch: {: 10}    Roll: {: >10}'.format(eul_ang[0]/16, eul_ang[1]/16, eul_ang[2]/16))
   
    def get_omega(self):
        self.i2c.mem_read(self.omeg, 0x28, 0x14)           #Write data to array
        ang_vel = [0,0,0]
        ang_vel[0] = self.omeg[1]<<8|self.omeg[0]
        ang_vel[1] = self.omeg[3]<<8|self.omeg[2] 
        ang_vel[2] = self.omeg[5]<<8|self.omeg[4]
        for i in range(3):
            if ang_vel[i] > 32767:
                ang_vel[i] -= 65536
       # print('Angular Velocities (rad/s): Omega_x: {:}  Omega_y: {:}  Omega_z: {:}'.format(ang_vel[0]/900, ang_vel[1]/900, ang_vel[2]/900) )
        
    def check_calib(self):
        
        out = [False, False, False, False]
    
        SYS = 0b11000000
        GYR = 0b00110000
        ACC = 0b00001100
        MAG = 0b00000011
        
        if (self.stat[0] & SYS) == SYS:
            out[0] = True
        if (self.stat[0] & GYR) == GYR:
            out[1] = True    
        if (self.stat[0] & ACC) == ACC:
            out[2] = True  
        if (self.stat[0] & MAG) == MAG:
            out[3] = True
        return out
    

if __name__ == '__main__':
    
    i2c = I2C(1, I2C.MASTER)
    imu = BNO055(i2c)
    imu.update(12)
    
    print('Calibrating: [SYS, GYR, ACC, MAG]')
    
    while True:
        binary = imu.calib_stat()
        print(str(imu.check_calib())+ "\t" + str(binary))
        if(binary == 0b11111111):
            imu.readcalib_coef()
            print('Calibration Complete')
            break
        utime.sleep(2)
        
    while True:
        imu.get_angle()
        imu.get_omega()
        utime.sleep(1)
        
        