# -*- coding: utf-8 -*-
''' @file           DRV8847.py
    @brief          Motor Driver
    @details        Initializes motor variables and commands 
    @details        File is imported by main.py
                    Click below to acces the source code:
                    https://bitbucket.org/dcejagon/me305_labs/src/master/Lab4/DRV8847.py


    @author:        Daniel Ceja
    @author:        Will Roy Halvorsen

''' 

import pyb
import utime
import shares


class Motor:
    ''' @brief      A motor class for one channel of the DRV8847.
        @details    Objects of this class can be used to apply PWM to a gien DC motor.
    '''
    
    def __init__ (self, tim_n, ch1, ch2, pin1, pin2):
        
        ''' @brief      Initilizes and runs a motor object associated with the DRV8847
            @details    Objects of this class should not be instantiated directly.
                        Instead create a DRV8847 object and use that to create
                        Motor objects using the method DRV8847.motor()
        ''' 
        self.tim = pyb.Timer(tim_n, freq=20000)
        
        self.in1 = self.tim.channel(ch1, pyb.Timer.PWM, pin = pin1) 
        self.in2 = self.tim.channel(ch2, pyb.Timer.PWM, pin = pin2)
        
       
        pass
    
    def set_duty (self, duty):
        
        ''' @brief      Sets PWM duty cycle for motor
            @details    Method sets duty cycle of the motors to given value 
                        where a positive value turns the motors clockwise and
                        negative value turn in counterclockwise. A zero value
                        would not spin the motor. 
            @param      duty    A signed number holding the duty cycle of the
                                PWM signal sent to the motor
        '''
        if (duty > 100 or duty < -100):
              #  print("Input value between -100 (ccw) and 100 (cw): {:}\n" .format(duty))
           pass
        # if Negative, motor will spin Counter Clockwiwe
        elif duty < 0:
            self.in1.pulse_width_percent(0) 
            self.in2.pulse_width_percent(-duty) 
       
       # if Positive motor will spin Clockwise
        elif duty > 0:
            self.in1.pulse_width_percent(duty) 
            self.in2.pulse_width_percent(0) 
       
        # if Zero, motor will not spin
        else:
            self.in1.pulse_width_percent(0) 
            self.in2.pulse_width_percent(0)
           
        
        
if __name__ == '__main__':
    
    f_flag = shares.Share(0)
    motor_drv       = DRV8847(3, pyb.Pin(pyb.Pin.cpu.A15), pyb.Pin(pyb.Pin.cpu.B2), f_flag =1 )
    motor_1         = motor_drv.motor(1)
    motor_2         = motor_drv.motor(2)
    
    # Enable the motor driver
    motor_drv.enable()
    
    # Set the duty cycle of the first motor to 50 percent and the duty cycle 
    # of the second motor to 50 percent
    motor_1.set_duty(50)
    motor_2.set_duty(50)
    
    # nFault = pyb.ExtInt(pyb.Pin.cpu.B2 ,mode=pyb.ExtInt.IRQ_FALLING, 
    #                        pull=pyb.Pin.PULL_NONE,callback=motor_drv.fault_cb)
    
    