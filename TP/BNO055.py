# -*- coding: utf-8 -*-
''' @file           BNO055.py
    @brief          IMU driver
    @details        Initializes IMU variables and commands 
    @details        File is imported by main.py
                    Click below to acces the source code:
                    https://bitbucket.org/dcejagon/me305_labs/src/master/Lab4/DRV8847.py

    @author:        Daniel Ceja
    @author:        Will Roy Halvorsen

''' 
from pyb import I2C
import utime




class BNO055:
    ''' @brief      A IMU class for the BNO055 IMU 
        @details    Objects of this class can be used to retrieve IMU data
    '''
        
    def __init__(self, i2c):
        ''' @brief      Initilizes an IMU object
            @details    Bytearray lengths are preallocated based off the IMU datasheet
        ''' 
        self.i2c = i2c
        self.angle = bytearray(6)            #Preallocate arrays to store data
        self.omeg = bytearray(6)
        self.coef = bytearray(22)
        self.stat = bytearray(1)
  
        

    def select_u(self, units):
        ''' @brief      Sets the units to be used for the output
            @details    Units set to radians
            @param      Units to be used
        '''         
        mode = self.i2c.mem_read(1, 40, 0x3D)
        self.i2c.mem_write(0, 40, 0x3D)
        self.i2c.mem_write(units, 40, 0x3B)
        self.i2c.mem_write(mode, 40, 0x3D)



    def update(self,mode):
        ''' @brief      Updates the IMU mode
            @details    Sets the mode of the IMU based on the IMU datasheet
            @param mode The mode of the IMU
        ''' 
        self.i2c.mem_write(mode,0x28,0x3D) 
        
    def calib_stat(self):
        ''' @brief      Returns the calibration status of the IMU
            @return     The calibration status
        ''' 
        self.i2c.mem_read(self.stat,0x28,0x35)
        return self.stat[0]
    
    def readcalib_coef(self):
        ''' @brief      Returns the calibration coefficient of the IMU
            @return     The calibration coefficient
        ''' 
        self.i2c.mem_read(self.coef,0x28,0x55)
        return self.coef
        
    def writecalib_coef(self, coefficients):
        ''' @brief      Writes the calibration coefficients to the IMU
        ''' 
        self.i2c.mem_write(coefficients,0x28,0x55)
        
    def get_angle(self):
        ''' @brief      Returns the Euler angles read by the IMU
            @return     The Euler angles in radians
        ''' 
        self.i2c.mem_read(self.angle, 0x28, 0x1A)           #Write data to array
        eul_ang = [0,0,0]
        eul_ang[0] = self.angle[1]<<8|self.angle[0]
        eul_ang[1] = self.angle[3]<<8|self.angle[2] 
        eul_ang[2] = self.angle[5]<<8|self.angle[4]
        for i in range(3):
            if eul_ang[i] > 32767:
                eul_ang[i] -= 65536
         
        return (eul_ang[0]/900, eul_ang[1]/900, eul_ang[2]/900)

        #print('Euler Angles (deg): Heading: {: >10}    Pitch: {: 10}    Roll: {: >10}'.format(eul_ang[0]/16, eul_ang[1]/16, eul_ang[2]/16))
   
    def get_omega(self):
        ''' @brief      Returns the Euler angle velocities read by the IMU
            @return     The Euler angle velocities in radians/s
        ''' 
        self.i2c.mem_read(self.omeg, 0x28, 0x14)           #Write data to array
        ang_vel = [0,0,0]
        ang_vel[0] = self.omeg[1]<<8|self.omeg[0]
        ang_vel[1] = self.omeg[3]<<8|self.omeg[2] 
        ang_vel[2] = self.omeg[5]<<8|self.omeg[4]
        for i in range(3):
            if ang_vel[i] > 32767:
                ang_vel[i] -= 65536
                
        return (ang_vel[0]/900, ang_vel[1]/900, ang_vel[2]/900)
       # print('Angular Velocities (rad/s): Omega_x: {:}  Omega_y: {:}  Omega_z: {:}'.format(ang_vel[0]/900, ang_vel[1]/900, ang_vel[2]/900) )
        
    def check_calib(self):
        ''' @brief      Returns the IMU calibration status of each component
            @return     The calibration status of each component
        ''' 
        
        out = [False, False, False, False]
    
        SYS = 0b11000000
        GYR = 0b00110000
        ACC = 0b00001100
        MAG = 0b00000011
        
        if (self.stat[0] & SYS) == SYS:
            out[0] = True
        if (self.stat[0] & GYR) == GYR:
            out[1] = True    
        if (self.stat[0] & ACC) == ACC:
            out[2] = True  
        if (self.stat[0] & MAG) == MAG:
            out[3] = True
        return out
    
    def calibrate(self):
        ''' @brief      Checks for IMU calibration coefficients already on the IMY
            @details    The IMU will either use the prewritten IMU calibration coefficients or proceed
                        to calibrate itself and write calibration coefficients to the IMU. This allows 
                        the IMU to not have to calibrate each time it operates
        ''' 
        print('IMU IS CALIBRATING...')
        
        filename = 'IMU_cal_coeff.txt'
        
        self.update(12)
        self.select_u(6)
        
        try:
            with open(filename, 'r') as f:
                cal_data_string = f.readline()
                
                arr = cal_data_string.strip().split(',')
                arrayOfBytes = bytearray(22)
                for i in range(22):
                    arrayOfBytes[i] = int(arr[i], 16)
                self.writecalib_coef(arrayOfBytes)
        
        except:
            print('Move Board')
            print('[SYS, GYR, ACC, MAG]')  #Required??
            nxt_time = utime.ticks_us() + 1000000
            while True:
                if(utime.ticks_us() >= nxt_time):
                    print(str(self.check_calib()) + "\t" + str(self.calib_stat()))
                    nxt_time = utime.ticks_us() + 1000000
                if(self.calib_stat() == 0b11111111):
                    self.writecalib_coef(self.readcalib_coef())
                    cal_data_string = ''
                    
                    for coef in self.coef:
                        cal_data_string += hex(coef) + ", "

                    cal_data_string = cal_data_string[:-2]

                    with open(filename, 'w') as f:
                        f.write(f"{cal_data_string}\r\n")
                    break

        print("CALIBRATION FOR IMU DONE")
       

if __name__ == '__main__':
    
    i2c = I2C(1, I2C.MASTER)
    imu = BNO055(i2c)
    imu.update(12)
    
    print('Calibrating: [SYS, GYR, ACC, MAG]')
    
    while True:
        binary = imu.calib_stat()
        print(str(imu.check_calib())+ "\t" + str(binary))
        if(binary == 0b11111111):
            imu.readcalib_coef()
            print('Calibration Complete')
            break
        utime.sleep(2)
        
    while True:
        imu.get_angle()
        imu.get_omega()
        utime.sleep(1)
        
        