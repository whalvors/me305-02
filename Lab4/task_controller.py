# -*- coding: utf-8 -*-
'''@file            task_controller.py
   @brief           Defines a task to run a controller driver
   @details         This file creates a task to run a closed loop controller driver at a regular interval
   @author Daniel Ceja Gonzalez
   @author Will Halvorsen
   @copyright License Info
   @date 11/6/2021
'''
import utime
'''@brief           Imports the utime module used to control the timing
'''
import math
'''@brief           Imports the math module used in the unit conversion of the measured angular velocity of the motor
'''


class Controller:
    '''@brief          Creates the Controller task
       @details        Updates the closed loop driver with measured encoder values
    '''    
    def __init__(self, freq, step_f, closed_loop, duty, delta, alg, start_T, stop_T, goal, kp = 0, ki = 0, kd = 0):
        '''@brief          Initializes the Controller task
         @details        Runs and updates the closed loop driver at a regular interval
         @param          freq The frequency to update the closed loop driver at
         @param          step_f The step frequency 
         @param          closed_loop The closed loop driver
         @param          duty The duty cycle of the motor
         @param          delta The change in position of the motor
         @param          alg The alg for the gains
         @param          start_T The start time flag
         @param          stop_T  The stop time flag
         @param          goal The goal/reference motor speed
         @param          kp The proportional gain
         @param          ki The integral gain
         @param          kd The differential gain
        '''
        self.freq=freq
        self.step_f=step_f
        self.closedloop=closed_loop
        self.duty=duty
        self.delta=delta
        self.alg=alg
        self.start_T=stop_T
        self.goal=goal
        self.kp=kp
        self.ki=ki
        self.kd=kd
        self.period = 1/freq * 1000000
        self.nextT = utime.ticks_us() + self.period
        
    def run(self):
        '''@brief Updates the closed loop control with measured motor information
           @details Continually updates the closed loop control by writing measured values to a share
        '''
        if utime.ticks_us() >= self.nextT:
            self.nextT = utime.ticks_us() + self.period
            if self.step_f.read():
                elapsedT = utime.ticks_dif(utime.ticks_us(),self.start_T.read())
                if elapsedT < 1000000:
                    speed = self.delta.read() * 2.0 * math.pi /4000 * self.freq
                    self.duty.write(self.closedloop.update(self.alg.read(), self.ref.read(), speed, self.kp.read(), self.ki.read(),self.kd.read()))
                else:
                    self.stop_T.write(1)
                    
                                    