'''@file            encoder.py
   @brief           Defines a driver to run a generalized encoder
   @details         This file creates a driver to run any encoder. It is to be called in another function 
   @author Daniel Ceja Gonzalez
   @author Will Halvorsen
   @copyright License Info
   @date 10/6/2021
'''
import pyb
'''@brief           Imports the pyb module used to control the nucleo
'''

class Encoder:
    '''@brief           Creates a class for a general encoder
       @details         Encoder creates a class that can be called to represent a general encoder
       '''
    
    
    def __init__(self,timnum,pin1,pin2): 
      '''@brief          Initializes the encoder class
         @details        Takes the timer number and the pin numbers to be used for the encoder to create the encoder class
         @param          The timer number for the encoder
         @param          The pin to be used for channel 1 of the encoder
         @param          The pin to be used for channel 2 of the encoder
       '''
   
      self.tim = pyb.Timer(timnum, period = 65535, prescaler = 0)
      self.timch1 = self.tim.channel(1, pyb.Timer.ENC_A, pin = pin1)
      self.timch2 = self.tim.channel(2, pyb.Timer.ENC_B, pin = pin2)

      self.pstart = 0
      
      self.p = 0
             

      print('Creating encoder object')
    
    def update(self):
        '''@brief           Updates the position of the encoder
           @details         Creates a function to update the position of the encoder and correct for over and underflow
        '''
       
#        while True:
            
            
        count = self.tim.counter()
        self.pstop = abs(count)
        
        self.d =  self.pstart - self.pstop
       
    
        if self.d > 32768:      # Underflow Condition 
            self.d -= 65535
        elif self.d < -32768:   # Overflow Condition  
            self.d += 65535
        
        self.p += self.d
       
        self.pstart = self.pstop 
        return self.p
#                print ('Encoder Position is {:}'.format(self.p))
        print('Reading encoder count and updating position and delta values')
        
    def get_position(self):
        '''@brief           Returns the encoder position
        '''
        return self.p
    
    def set_position(self,position):
        '''@brief           Sets the position of the encoder
         @details         Sets the position of the encoder to the user specified position
         @param           The user specified position 
        '''
        self.pstart = 0
        self.p = position
        self.d = 0
        self.pstop = 0
        print('Setting position and delta values')
        
    def get_delta(self):
        '''@brief           Returns the change in encoder position
        '''
        
        return self.d
