# -*- coding: utf-8 -*-
"""@file        main.py
@brief          Instatiates variables and run tasks simultaneously
@details        File calls other task to run on ball balancing system
                Click below to access most recent the source code for Term Project:
                https://bitbucket.org/dcejagon/me305_labs/src/master/Term_Project/main.py

@author:        Daniel Ceja
@author:        Will Roy Halvorsen
@date:          December 9, 2021
"""

import task_user
import DRV8847
from pyb import Pin, I2C
import shares
import task_controller
import task_touch
import touch_panel
import BNO055
import task_IMU
from ulab import numpy as np

if __name__ == '__main__':
    
    '''
    @brief       Instantiates objects used on the ball balancing system
    @details     IMU calibration data and Data collection shares set up
    '''

    Gain_ks = np.array([-.3, -.2, -.05, -.02])  # [N, N*m, N*s, N*m*s]
    state_t = shares.Share(())      # State: Touch
    state_i = shares.Share(())      # State: Imu
    
    t_queue = shares.Queue()      # Queue: Touch
    i_queue = shares.Queue()    # Queue: IMU
    td_queue = shares.Queue()          # Queue: Duty/Time
    
    balance = shares.Share(0)
    collection = shares.Share(0)
    
    stop_d = shares.Share(0)
    start_T = shares.Share(0)   
   
    
    freq_input = 200    # touch panel, IMU task
    freq_user = 50      # user task
    freq_imu = 100   # IMU task
    
    '''@brief   Sets frequency for User and IMU for controller
    '''
    
    T_panel  = touch_panel.Touch_Panel(Pin.cpu.A7, Pin.cpu.A1, Pin.cpu.A6, Pin.cpu.A0)
    '''@brief   Initialization of Touch Panel
    '''
    T_panel.calibrate()
    
    BNO055 = BNO055.BNO055(I2C(1, I2C .MASTER))
    '''
    @brief      Initialization of IMU
    '''
    BNO055.calibrate()
    
    mot_1 = DRV8847.Motor(3, 1, 2, Pin(Pin.cpu.B4), Pin(Pin.cpu.B5))                     # Motor 1
    '''@brief   Initialization of pin motor object for theta_y 
    '''
    mot_2 = DRV8847.Motor(3, 3, 4, Pin(Pin.cpu.B0), Pin(Pin.cpu.B1))                     # Motor 2
    '''@brief   Initialization of pin motor object for theta_x 
    '''
   
    user_init = task_user.Task_user(start_T, stop_d, td_queue, t_queue, i_queue, balance, collection, freq_user)
    '''@brief   Initialization of user objecto for collection queues
    '''
    
    con = task_controller.Controller(freq_input, collection, balance, stop_d, start_T, mot_1, mot_2,
                                     Gain_ks, state_i, state_t, td_queue, t_queue, i_queue)
    '''@brief   Initialization of controller object for motor input
                and collection queues
    '''
    Panel = task_touch.Task_touch( freq_input, T_panel, collection, balance, state_t)
    '''@brief   Initialization of touch panel object
    '''
    imu = task_IMU.Task_imu(freq_imu, BNO055, state_i, balance, collection)
    '''@brief   Initialization of IMU
    '''
   
    while(True):
           try:
               '''
               @brief       Run tasks simultaneously
               '''
    
               imu.run()
               Panel.run()
               con.run()
               user_init.run()
           except KeyboardInterrupt:
                break
    print('Ending Program')